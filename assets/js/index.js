// var Person = function(first_name, last_name, email) {
//     return({
//         first_name: first_name,
//         last_name: last_name,
//         email: email,
//         printName: function() {
//             return first_name + " " + last_name;
//         }
//     });
// };
// Person.prototype.printEmail = function() {
//     return this.email;
// }

// var Person2 = function(first_name, last_name, email) {
//     this.first_name = first_name;
//     this.last_name = last_name;
//     this.email = email;
//     this.printName = function() {
//         return (this.first_name + " " + this.last_name);
//     }
// };
// Person2.prototype.printEmail = function() {
//     return this.email;
// }

// var fred = Person("fred", "flintstone", "fred@bedrock.com");
// var wilma = new Person2("wilma", "flintstone", "wilmad@bedrock.com")    


// var peopleArray = [];
// peopleArray.push(fred);
// peopleArray.push(wilma);


(function() {
    var Day06App = angular.module("Day06App", []);

    var Day06Ctrl = function() {
        var day06Ctrl = this;

        day06Ctrl.item = "";
        day06Ctrl.quantity = 0;
        day06Ctrl.filterText = "";

        // Default items in cart
        day06Ctrl.cart = [
            {
                item: 'APPLES',
                quantity: 12
            },
            {
                item: 'PEARS',
                quantity: 10
            }
        ];

        day06Ctrl.addItem = function() {
            var index = day06Ctrl.cart.findIndex(
                function(element) {
                    return (element.item == day06Ctrl.item.toUpperCase());
                }
            );

            if(index == -1) {
                day06Ctrl.cart.push({
                        'item': day06Ctrl.item.toUpperCase(),
                        'quantity': day06Ctrl.quantity
                });
            }
            else {
                day06Ctrl.cart[index].quantity += day06Ctrl.quantity;
            }

            day06Ctrl.item = "";
            day06Ctrl.quantity = 0;
        }

        day06Ctrl.delItem = function(item) {
            var index = day06Ctrl.cart.findIndex(function(element) {
                return (item == element.item);
            });
            if(index >= 0) {
                day06Ctrl.cart.splice(index, 1);
            }
        }
    };

     Day06App.controller("Day06Ctrl", [ Day06Ctrl ]);
})();