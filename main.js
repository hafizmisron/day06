// Load libraries
var express = require("express");
var path = require("path");


// Create instance of Express
var app = express();

// Set the port to 5000 default
app.set("port", parseInt(process.env.APP_PORT) || parseInt(process.argv[2]) || 5000);


// Routes
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "assets")));
app.use("/lib", express.static(path.join(__dirname, "/bower_components")));


// Start the app
app.listen(app.get("port"), function() {   
    console.log("Day 06 app started at %s at port %d", new Date(), app.get("port"));
});
